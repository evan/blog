# Build Image
FROM squidfunk/mkdocs-material:latest as build

# Build MkDocs
COPY . /app
WORKDIR /app
RUN pip install -U -r ./requirements.txt
RUN mkdocs build

# Main Image - Copy Resources
FROM nginx
COPY --from=build /app/site /usr/share/nginx/html
